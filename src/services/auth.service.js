import axios from "axios";
import {ADMIN_URL} from '../constants/index'

const register = (userName, email, password, mobileNumber) => {
  const body = {
    userName,
    email,
    password,
    mobileNumber
  }
  return axios.post(ADMIN_URL + "register",body);
};

const login = (email, password) => {
  const body={
    email,
    password,
  }
  return axios.post(ADMIN_URL + "login", body)
    .then((response) => {
      if (response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

export default {
  register,
  login,
  logout,
};
