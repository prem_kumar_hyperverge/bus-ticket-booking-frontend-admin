import axios from "axios";
import authHeader from "./auth-header";
import {ADMIN_URL} from '../constants/index'

const getUserBoard = () => {
  return axios.get(ADMIN_URL + "user", { headers: authHeader() });
};

const getBusDetails = (id) => {
  const body ={
    "userId": id
  }
  return axios.post(ADMIN_URL + "viewBuses", body, { headers: authHeader() });
};

const resetBus = (busId) => {
  const body = {
    "busId":busId 
  }
  return axios.post(ADMIN_URL + "resetSeats", body, { headers: authHeader() });
};
const deleteBus = (busId) => {
  const body = {
    "busId":busId 
  }
  return axios.post(ADMIN_URL + "removeBuses", body, { headers: authHeader() });
};

const addBus = (name,sourceCity,destinationCity,fare,pickupLocation,dropLocation,busNumber,seatCount,departureTime,arrivalTime,date,id,busImage) => {
  const body = { 
  "busName":name,
  "travelDate":date,
  "userId":id,
  "sourceCity":sourceCity,
  "destinationCity":destinationCity,
  "fare":fare,
  "departureTime": departureTime,
  "arrivalTime":arrivalTime,
  "noOfSeats":seatCount,
  "busNumber":busNumber,
  "pickupLocation":pickupLocation,
  "dropLocation":dropLocation,
  "busImage":busImage
  };
  return axios.post(ADMIN_URL + "addBuses",body , { headers: authHeader() });
};

export default {
  getUserBoard,
  getBusDetails,
  resetBus,
  deleteBus,
  addBus
};