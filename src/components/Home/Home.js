import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Link } from 'react-router-dom';
import './Home.css'
import BusCards from '../BusCards/BusCards'
import { logout } from "../../actions/auth";
const Home = (props) => {
  const { isLoggedIn } = useSelector(state => state.LoggedIn);
  const dispatch = useDispatch();
  const logOut = () => {
    props.history.push("/login");
    dispatch(logout());
  };
  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }
  return (
    <div>
      <div class="navbar">
        <a class="logo" href="#"> HyperBus</a>
        <ul class="nav">
          <li><Link to="/addBus"><a href="#about">Add Bus</a></Link></li>&ensp;&ensp;
          <li><a onClick={logOut} class="logout">Logout</a></li>
        </ul>
      </div>
      <section class="about-area" id="about">
        <div class="text-part">
          <p>
            <BusCards />
          </p> </div>
      </section>
    </div>
  );
};

export default Home;
