import React, { useState } from "react";
import { useDispatch } from "react-redux";
import swal from 'sweetalert';
import './Register.css'
import { register } from "../../actions/auth";

const Register = (props) => {
  const [userName, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const dispatch = useDispatch();
  const onChangeUsername = (e) => {
    const userName = e.target.value;
    setUsername(userName);
  };
  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };
  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };
  const onChangeMobile = (e) => {
    const mobileNumber = e.target.value;
    setMobileNumber(mobileNumber);
  };
  const handleRegister = (e) => {
    if (!userName || !email || !password || !mobileNumber) {
      swal({
        title: 'All fields are mandatory',
        icon: 'warning',
        timer: 2000,
        buttons: true,
      })
    }
    else {
      e.preventDefault();
      dispatch(register(userName, email, password, mobileNumber))
        .then((res) => {
          swal({
            title: 'Register Success',
            icon: 'success',
            timer: 2000,
            buttons: true,
          })
          props.history.push("/login");
          window.location.reload();
        })
        .catch((err) => {
          swal({
            title: 'Email Id already exists',
            icon: 'error',
            timer: 2000,
            buttons: true,
          })
        });
    }
  };
  const openSignIn = () => {
    props.history.push("/login");
  }
  return (
    <div>
      <div className="container" id="container">
        <div className="form-container sign-in-container">
          <div className="form-control">
            <h1>Create Account</h1>
            <input type="text" value={userName} placeholder="Name"
              onChange={onChangeUsername} />
            <input type="email" value={email} placeholder="Email"
              onChange={onChangeEmail} />
            <input type="password" value={password} placeholder="Password"
              onChange={onChangePassword} />
            <input type="text" value={mobileNumber} placeholder="Mobile Number"
              onChange={onChangeMobile} />
            <button onClick={handleRegister}>Register</button>
          </div>
        </div>
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-right">
              <h1>Welcome Back!</h1>
              <p>To keep connected with us please login with your personal info</p>
              <button className="ghost" id="signIn" onClick={openSignIn}>Sign In</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
