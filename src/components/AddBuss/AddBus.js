import React, { useState } from "react";
import { useSelector } from "react-redux";
import FileBase64 from 'react-file-base64';
import TimePicker from 'react-time-picker';
import './AddBus.css';
import data from '../../constants/cities-name-list.json'
import { Redirect } from 'react-router-dom';
import UserService from "../../services/user.service";

const AddBus = (props) => {
	const [name, setName] = useState("");
	const [sourceCity, setSourceCity] = useState("Chennai");
	const [destinationCity, setDestinationCity] = useState("Coimbatore");
	const [fare, setFare] = useState("");
	const [seatCount, setSeatCount] = useState("40");
	const [pickupLocation, setPickupLocation] = useState("");
	const [dropLocation, setDropLocation] = useState("");
	const [busNumber, setBusNumber] = useState("");
	const [travelDate, setTravelDate] = useState([]);
	const [busImage, setBusImage] = useState('');
	const [departureTime, setDepartureTime] = useState('09:00');
	const [arrivalTime, setArrivalTime] = useState('06:00');
	const { isLoggedIn } = useSelector(state => state.auth);
	const { id } = useSelector(state => state.auth.user.data.admin);

	const onChangeName = (e) => {
		const name = e.target.value;
		setName(name);
	};
	const onChangeSourceCity = (e) => {
		const sourceCity = e.target.value;
		setSourceCity(sourceCity);
	}
	const onChangeDestinationCity = (e) => {
		const destinationCity = e.target.value;
		setDestinationCity(destinationCity);
	}
	const onChangeFare = (e) => {
		const fare = e.target.value;
		setFare(fare);
	}
	const onChangePickupLocation = (e) => {
		const pickupLocation = e.target.value;
		setPickupLocation(pickupLocation);
	}
	const onChangeDropLocation = (e) => {
		const dropLocation = e.target.value;
		setDropLocation(dropLocation);
	}
	const onChangeBusNumber = (e) => {
		const busNumber = e.target.value;
		setBusNumber(busNumber);
	}
	const onChangeTravelDate = (e) => {
		const travelDate = e.target.value;
		setTravelDate(travelDate);
	}
	function convertDate(dateString) {
		var p = dateString.split(/\D/g)
		return [p[2], p[1], p[0]].join("/")
	}

	const onTimeChangeDep = (time) => {
		setDepartureTime(time);
	}
	const onTimeChangeArr = (time) => {
		setArrivalTime(time);
	}
	const addBus = async () => {
		let date = convertDate(travelDate)
		if (!name || !sourceCity || !destinationCity || !fare || !pickupLocation || !dropLocation || !busNumber || !seatCount || !departureTime || !arrivalTime || !date || !id || !busImage) {
			alert("Please enter all fields to proceed");
		}
		if (sourceCity === destinationCity) {
			alert("Source and Destination City cannot be same:)");
		}
		else {
			await UserService.addBus(name, sourceCity, destinationCity, fare, pickupLocation, dropLocation, busNumber, seatCount, departureTime, arrivalTime, date, id, busImage).then(
				async (response) => {
					await alert("Bus added Successfully");
				},
				(error) => {
					alert("Something went wrong, please try again");
				}
			);
		}
	}
	const getFiles = async (files) => {
		setBusImage(files.base64);
	}
	const returnHome = () => {
		props.history.push("/");
	};
	if (!isLoggedIn) {
		return <Redirect to="/login" />;
	}
	return (
		<div>
			<center>
				<div className="container-add" id="container">
					<div className="form-container sign-in-container">
						<div className="form-control">
							<h1>Add Bus</h1>
							<input type="text" placeholder="Enter your Bus Name" value={name}
								onChange={onChangeName} />
							<div className="main-div">
								<div className="left-div">
									<span class="form-label">Source City</span>
								</div>
							</div>
							<select value={sourceCity} onChange={onChangeSourceCity} className="select-div">
								{data.map((value, index) => (
									<option value={value}>{value}</option>
								))}
							</select>
							<div className="main-div" >
								<div className="left-div">
									<span class="form-label">Destination City</span>
								</div>
							</div>
							<select value={destinationCity} onChange={onChangeDestinationCity} className="select-div">
								{data.map((value, index) => (
									<option value={value}>{value}</option>
								))}
							</select>
							<input type="text" placeholder="Fare" value={fare}
								onChange={onChangeFare} />
							<input type="text" placeholder="Enter the Seat Count" value="40" disabled
							/>
							<input type="text" placeholder="Enter the Pickup Location" value={pickupLocation}
								onChange={onChangePickupLocation} />
							<input type="text" placeholder="Enter the Drop Location" value={dropLocation}
								onChange={onChangeDropLocation} />
							<input type="text" placeholder="Enter Bus number" value={busNumber}
								onChange={onChangeBusNumber} />
							<input type="date" required value={travelDate} min='2021-01-27'
								onChange={onChangeTravelDate} />
							<div className="main-div">
								<div className="left-div">
									<span >Departure</span>
									<br />
									<TimePicker onChange={onTimeChangeDep} value={departureTime} disableClock={true} className="time-div" />

								</div>
								<div className="right-div">
									<span>Arrival</span>
									<br />
									<TimePicker onChange={onTimeChangeArr} value={arrivalTime} disableClock={true} className="time-div" />
								</div>
								<FileBase64 id="busImage" name="busImage" required onDone={getFiles.bind(this)} />
							</div>
							<br />
							<br />
							<button onClick={addBus}>ADD</button>
						</div>
					</div>
					<div className="overlay-container">
						<div className="overlay">
							<div className="overlay-panel overlay-right">
								<h1>Hello, admin!</h1>
								<p>Enter your bus details and start journey with us, let's earn together</p>
								<button className="ghost" id="signUp" onClick={returnHome}>Home</button>
							</div>
						</div>
					</div>
				</div>
			</center>
		</div>
	);
};

export default AddBus;
