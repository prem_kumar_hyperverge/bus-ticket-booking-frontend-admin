import React, { useState, useEffect } from "react";
import UserService from "../../services/user.service";
import "./BusCards.scss";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import { Card, CardText, CardBody, Button, Row } from "reactstrap";
import { Col, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import CardDetails from "../Card/Card";
import SeatArrangement from "../SeatArrangement/SeatArrangement";

const BusCards = () => {
  const [content, setContent] = useState([]);
  const [modal, setModal] = useState(false);
  const [view, setView] = useState(false);
  const [visible, setVisible] = useState(false);
  const [fare, setFare] = useState("");
  const [busId, setBusId] = useState("");
  const [busNumber, setBusNumber] = useState("");
  const [seatNumber, setSeatNumber] = useState("");
  const [seatEmail, setSeatEmail] = useState("");
  const [seatName, setSeatName] = useState("");
  const [seatMobileNumber, setSeatMobileNumber] = useState("");
  const [seatGender, setSeatGender] = useState("");
  const [seatArrangement, setSeatArrangement] = useState([]);
  const { id } = useSelector((state) => state.auth.user.data.admin);
  useEffect(async () => {
    getDetails();
  }, []);

  const getDetails = async () => {
    await UserService.getBusDetails(id).then(
      async (response) => {
        await setContent(response.data);
        setVisible(true);
      },
      (error) => {
        swal({
          title: "Something went wrong, please reload the page",
          icon: "error",
          timer: 2000,
          buttons: true,
        });
      }
    );
  };
  const openModal = async (seats, fare, busNumber, id) => {
    setView(false);
    setBusId(id);
    setSeatArrangement(seats);
    setFare(fare);
    setBusNumber(busNumber);
    setModal(true);
  };
  const closeModal = async () => {
    setModal(false);
  };
  const viewDetails = async (
    sNumber,
    sEmail,
    sName,
    sMobileNumber,
    sGender,
    sReserved
  ) => {
    if (sReserved) {
      setSeatEmail(sEmail);
      setSeatNumber(sNumber);
      setSeatName(sName);
      setSeatMobileNumber(sMobileNumber);
      setSeatGender(sGender);
      setView(true);
    } else {
      setView(false);
    }
  };
  const resetBus = async () => {
    await UserService.resetBus(busId).then(
      async (response) => {
        getDetails();
        setView(false);
        setModal(false);
        swal({
          title: "Tickets are reseted",
          icon: "success",
          timer: 2000,
          buttons: true,
        });
      },
      (error) => {
        swal({
          title: "Something went wrong, please reload the page",
          icon: "error",
          timer: 2000,
          buttons: true,
        });
        getDetails();
        setModal(false);
      }
    );
  };
  const deleteBus = async (busId) => {
    await UserService.deleteBus(busId).then(
      async (response) => {
        swal({
          title: "Bus deleted",
          icon: "success",
          timer: 2000,
          buttons: true,
        });
        getDetails();
        setView(false);
      },
      (error) => {
        swal({
          title: "Something went wrong, please reload the page",
          icon: "error",
          timer: 2000,
          buttons: true,
        });
        getDetails();
        setModal(false);
      }
    );
  };

  return (
    <div>
      {visible === true ? (
        <div>
          {content.length != 0 ? (
            <div>
              {content.map((value, index) => (
                <Row className="row">
                  <CardDetails
                    value={value}
                    busDetails={openModal}
                    busDelete={deleteBus}
                  />
                  <Modal isOpen={modal} toggle={closeModal} size="xl">
                    <ModalHeader xl={12} lg={12} md={12} toggle={closeModal}>
                      View Passenger Seats
                    </ModalHeader>
                    <ModalBody>
                      <Row>
                        <SeatArrangement
                          seatArrangement={seatArrangement}
                          viewDetails={viewDetails}
                        />
                        <Col xl={6} lg={6} md={12}>
                          <Card className="passenger-div">
                            {view ? (
                              <CardBody>
                                <center>
                                  <CardText>Name : {seatName}</CardText>
                                  <CardText>Email : {seatEmail}</CardText>
                                  <CardText>Gender : {seatGender}</CardText>
                                  <CardText>
                                    Seat Number : {seatNumber}
                                  </CardText>
                                  <CardText>
                                    Mobile Number : {seatMobileNumber}
                                  </CardText>
                                </center>
                              </CardBody>
                            ) : (
                              <CardText>
                                <center>No Passengers has booked</center>
                              </CardText>
                            )}
                          </Card>
                        </Col>
                      </Row>
                      <center>
                        <Button
                          color="primary"
                          className="reset-div"
                          onClick={resetBus}
                        >
                          Reset Seats
                        </Button>
                      </center>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary" onClick={closeModal}>
                        Cancel
                      </Button>
                    </ModalFooter>
                  </Modal>
                </Row>
              ))}
            </div>
          ) : (
            <p>
              <center>No buses found</center>
            </p>
          )}
        </div>
      ) : null}
    </div>
  );
};

export default BusCards;
