import React from "react";
import "./Card.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBus,
  faCalendarAlt,
  faRupeeSign,
} from "@fortawesome/free-solid-svg-icons";
import { Card, CardImg, CardBody, Button, Row } from "reactstrap";
import { Col } from "reactstrap";
function CardDetails(props) {
  function openModal() {
    props.busDetails(
      props.value.seatDetails,
      props.value.fare,
      props.value.busNumber,
      props.value._id
    );
  }
  function deleteBus() {
    props.busDelete(props.value._id);
  }

  return (
    <Col md={12} sm={12} xs={12}>
      <Card className="flex-row" style={{ borderColor: "transparent" }}>
        <Col md={2} sm={2} xs={2}>
          {/* <CardImg
      className="card-img-left"
      src={props.value.busImage}
      style={{ width: 'auto', height: 310 }}
    /> */}
        </Col>
        <Col md={8} sm={8} xs={8}>
          <CardBody>
            <Row>
              <Col lg={12} md={12} sm={12} xs={12}>
                <Card className="card-style">
                  <CardBody>
                    <div className="cardbody-style">
                      <div className="left-div-font">
                        <FontAwesomeIcon
                          icon={faBus}
                          className="font-awesome-icons"
                        />{" "}
                        {props.value.busName}
                      </div>
                      <div className="side-div">
                        <FontAwesomeIcon
                          icon={faCalendarAlt}
                          className="font-awesome-icons"
                        />{" "}
                        {props.value.travelDate}
                      </div>
                      <br />
                      <div className="light-color">From</div>
                      <div className="dark-color">To</div>
                      <br />
                      <div className="font-float">{props.value.sourceCity}</div>
                      <div className="city-div">
                        {props.value.destinationCity}
                      </div>{" "}
                      <center>
                        <span className="center-div">
                          {" "}
                          {props.value.busNumber}
                        </span>
                      </center>
                      <div className="light-color">Departure Time</div>
                      <div className="dark-color">Arrival Time</div>
                      <br />
                      <div className="font-float">
                        {props.value.departureTime}
                      </div>
                      <div className="city-div">{props.value.arrivalTime}</div>
                      <center>
                        <p>
                          {" "}
                          <FontAwesomeIcon
                            icon={faRupeeSign}
                            className="font-rupee"
                          />{" "}
                          <span className="fare-div">{props.value.fare}</span>
                        </p>
                      </center>
                      <div className="cardbody-style">
                        <div className="view-div">
                          <Button color="primary" onClick={openModal}>
                            View Details
                          </Button>
                        </div>
                        <div className="delete-div">
                          <Button color="danger" onClick={deleteBus}>
                            Delete
                          </Button>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </CardBody>
        </Col>
      </Card>
    </Col>
  );
}
export default CardDetails;
