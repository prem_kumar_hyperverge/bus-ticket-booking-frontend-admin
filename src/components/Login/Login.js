import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import swal from 'sweetalert';
import './Login.css'
import { login } from "../../actions/auth";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { isLoggedIn } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };
  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };
  const handleLogin = (e) => {
    if (!email || !password) {
      swal({
        title: 'All fields are mandatory',
        icon: 'warning',
        timer: 2000,
        buttons: true,
      })
    }
    else {
      e.preventDefault();
      dispatch(login(email, password))
        .then((res) => {
          swal({
            title: 'Login Success',
            icon: 'success',
            timer: 2000,
            buttons: true,
          })
          props.history.push("/home");
          window.location.reload();
        })
        .catch((err) => {
          swal({
            title: 'Email or password wrong',
            icon: 'error',
            timer: 2000,
            buttons: true,
          })
        });
    }
  };
  const openSignUp = () => {
    props.history.push("/register");
  }
  if (isLoggedIn) {
    return <Redirect to="/home" />;
  }

  return (
    <div>
      <div className="container" id="container">
        <div className="form-container sign-in-container">
          <div className="form-control">
            <h1>Sign in</h1>
            <input type="email" value={email} placeholder="Email"
              onChange={onChangeEmail} />
            <input type="password" value={password} placeholder="Password"
              onChange={onChangePassword} />
            <button onClick={handleLogin}>Login</button>
          </div>
        </div>
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-right">
              <h1>Hello, admin!</h1>
              <p>Enter your personal details and start journey with us</p>
              <button className="ghost" id="signUp" onClick={openSignUp}>Sign Up</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
};

export default Login;
