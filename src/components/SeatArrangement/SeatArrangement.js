import './SeatArrangement.scss'
import { Col, Row } from 'reactstrap';

function SeatArrangement(props) {

    function viewDetails(number,email,userName,mobileNumber,gender,isReserved) {
        props.viewDetails(number,email,userName,mobileNumber,gender,isReserved);
    }
    return (
        <Col xl={6} lg={6} md={12} >
        <div className="seat-div">
          <div class="train">
            <li class="row row--1">
              <ol class="seats">
                <Row xs={4}>
                  {props.seatArrangement.map((value, index) => (
                    <Col>
                      <li class="seat">
                        <input type="checkbox" />
                        <label style={value.isReserved ? { backgroundColor: 'green' } : null} for={index} onClick={() => viewDetails(value.number, value.email, value.userName, value.mobileNumber, value.gender, value.isReserved)}>&nbsp;&nbsp;&nbsp;{value.id}&nbsp;&nbsp;&nbsp;</label>
                      </li>
                    </Col>
                  ))}
                </Row>
              </ol>
            </li>
          </div>
          <div class="exit exit--back train-body"></div>
        </div>
      </Col>
    )
}
export default SeatArrangement;